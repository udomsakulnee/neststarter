export interface Comment {
    id: string;
    detail: string;
}