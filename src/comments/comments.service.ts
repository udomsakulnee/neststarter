import { UpdateCommentInput } from './update-comment.input';
import { Comments } from './comment.entity';
import { CreateCommentInput } from './create-comment.input';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Comments)
    private commentsRepository: Repository<Comments>,
  ) {}

  findAll() {
    return this.commentsRepository.find();
  }

  findOne(id) {
    return this.commentsRepository.findOne(id);
  }

  create(body: CreateCommentInput) {
    const str = this.commentsRepository.create(body);
    return this.commentsRepository.save(str);
  }

  async update(id, body: UpdateCommentInput) {
    let comment = await this.commentsRepository.findOne(id);
    comment = this.commentsRepository.merge(comment, body);
    return this.commentsRepository.save(comment);
  }

  delete(id) {
    return this.commentsRepository.delete(id);
  }
}
