import { UpdateCommentInput } from './update-comment.input';
import { CreateCommentInput } from './create-comment.input';
import { CommentsService } from './comments.service';
import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';

@Controller('comments')
export class CommentsController {
  constructor(private service: CommentsService) {}

  @Get()
  findAll(): object {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id): object {
    return this.service.findOne(id);
  }

  @Post()
  create(@Body() body: CreateCommentInput) {
    return this.service.create(body);
  }

  @Patch(':id')
  update(@Param('id') id, @Body() body: UpdateCommentInput) {
    return this.service.update(id, body);
  }

  @Delete(':id')
  delete(@Param('id') id) {
    return this.service.delete(id);
  }
}
