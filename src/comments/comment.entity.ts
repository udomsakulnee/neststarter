import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from "typeorm";

@Entity()
export class Comments {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    detail: string;

    @UpdateDateColumn()
    updateDate: Date;

    @CreateDateColumn()
    createDate: Date;
}
