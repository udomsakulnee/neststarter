import { UpdateArticleInput } from './update-article.input';
import { CreateArticleInput } from './create-article.input';
import { ArticlesService } from './articles.service';
import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';

@Controller('articles')
export class ArticlesController {
  constructor(private service: ArticlesService) {}

  // /articles
  @Get()
  findAll(): object {
    return this.service.findAll();
  }

  // articles/1
  @Get(':id')
  findOne(@Param('id') id): object {
    return this.service.findOne(id);
  }

  @Post()
  create(@Body() body: CreateArticleInput) {
    return this.service.create(body);
  }

  @Patch(':id')
  update(@Param('id') id, @Body() body: UpdateArticleInput) {
    return this.service.update(id, body);
  }

  @Delete(':id')
  delete(@Param('id') id) {
    return this.service.delete(id);
  }
}
