import { ArticlesService } from './articles.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { Articles } from './article.entity';
import { ArticlesController } from './articles.controller';

@Module({
    imports: [TypeOrmModule.forFeature([Articles])],
    controllers: [ArticlesController],
    providers: [ArticlesService]
})
export class ArticlesModule {}
