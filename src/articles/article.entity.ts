import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from "typeorm";

@Entity()
export class Articles {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 120 })
    title: string;

    @Column()
    body: string;

    @UpdateDateColumn()
    updateDate: Date;

    @CreateDateColumn()
    createDate: Date;
}
