import { UpdateArticleInput } from './update-article.input';
import { Articles } from './article.entity';
import { CreateArticleInput } from './create-article.input';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectRepository(Articles)
    private articlesRepository: Repository<Articles>,
  ) {}

  findAll() {
    return this.articlesRepository.find();
  }

  findOne(id) {
    return this.articlesRepository.findOne(id);
  }

  create(body: CreateArticleInput) {
    const str = this.articlesRepository.create(body);
    return this.articlesRepository.save(str);
  }

  async update(id, body: UpdateArticleInput) {
    let article = await this.articlesRepository.findOne(id);
    article = this.articlesRepository.merge(article, body);
    return this.articlesRepository.save(article);
  }

  delete(id) {
    return this.articlesRepository.delete(id);
  }
}
